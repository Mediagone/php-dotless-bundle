<?php

/*
 * This file is part of the mediagone/dotless package.
 *
 * (c) Bruce Suire <bruce@mediagone.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mediagone\Dotless\Command;

use Mediagone\Dotless\Component\Compiler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * CompileCommand enables to compile project's LESS assets through command line.
 */
class CompileCommand extends Command
{
    //========================================================================================================
    // Properties
    //========================================================================================================
    
    /**
     * @var ContainerInterface The current ContainerInterface instance.
     */
    private $container;
    
    
    
    //========================================================================================================
    // Constructor
    //========================================================================================================
    
    /**
     * @param ContainerInterface $container The injected service container instance.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        
        parent::__construct();
    }
    
    
    
    //========================================================================================================
    // Methods
    //========================================================================================================
    
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('dotless:compile');
        
        // The short description shown while running "php bin/console list"
        $this->setDescription('Compile your LESS files into configurable outputs.');
        
        // The full command description shown when running the command with the "--help" option
        $this->setHelp(<<<'EOF'

'This command allows you to compile your LESS files.
EOF
        );
        
        $this->addOption('minify', null, null, 'Minify the output file?', null);
        
        $this->addArgument('sources', InputArgument::IS_ARRAY, 'Define JSON config files to compile.');
    }
    
    
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $sources= $input->getArgument('sources');
        $minified = $input->getOption('minify');
        $mapped = false;
        
        $kernel = $this->container->get('kernel');
        $cacheDir = $kernel->getCacheDir().DIRECTORY_SEPARATOR .'less'.DIRECTORY_SEPARATOR;
        $projectDir = $kernel->getProjectDir().DIRECTORY_SEPARATOR;
        
        $compiler = new Compiler($cacheDir, $projectDir, static function($msg) use ($output) { $output->writeln($msg); });
        $compiler->compileSources($sources, $minified, $mapped);
        
        return 0;
    }
    
    
    
}
