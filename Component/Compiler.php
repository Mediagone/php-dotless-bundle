<?php

/*
 * This file is part of the mediagone/dotless package.
 *
 * (c) Bruce Suire <bruce@mediagone.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mediagone\Dotless\Component;

use InvalidArgumentException;
use Less_Parser;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Compiler compiles project's LESS assets.
 */
class Compiler
{
    //========================================================================================================
    // Properties
    //========================================================================================================
    
    private $fs;
    
    private $output;
    
    private $cacheDir;
    private $projectDir;
    
    
    
    //========================================================================================================
    // Constructor
    //========================================================================================================
    
    public function __construct(string $cacheDir, string $projectDir, callable $output)
    {
        $this->fs = new Filesystem();
        $this->output = $output;
        $this->cacheDir = $cacheDir;
        $this->projectDir = $projectDir;
    }
    
    
    
    //========================================================================================================
    // Methods
    //========================================================================================================
    
    public function compileSources(array $sources, bool $minify, bool $sourceMap)
    {
        foreach ($sources as $source) {
            
            if (!$this->fs->exists($this->projectDir.$source)) {
                ($this->output)(' '.$source.' file not found.');
                continue;
            }
            
            $this->compile($source, $minify, $sourceMap);
        }
    }
    
    
    /**
     * Compiles project's LESS assets according to the dotless.json file.
     *
     * @param bool $minify Whether the output css must be minified or not.
     * @param bool $sourceMap Whether a source map must be generated or not.
     */
    public function compile(string $sourceFile, bool $minify, bool $sourceMap)
    {
        ($this->output)('--------------------------------------------------------------------');
        
        
        $compiler = new Less_Parser([
            'compress' => $minify,
            'cache_dir' => $this->cacheDir,
            //'sourceMap' => $sourceMap,
            //'sourceMapWriteTo'  => '/var/www/mysite/writable_folder/filename.map',
            //'sourceMapURL'      => '/mysite/writable_folder/filename.map',
        ]);
        
        
        ($this->output)(' '.$sourceFile.' found and successfuly loaded');
        
        try
        {
            $config = new Config((string)file_get_contents($this->projectDir.$sourceFile));
            ($this->output)(' > '.count($config->getTargets()).' targets found');
            
            
            foreach ($config->getTargets() as $target)
            {
                $files = $this->findCompilableSources($target);
                $globals = $this->findCompilableGlobals($target);
                
                ($this->output)('');
                ($this->output)('Compiling '.$target->getOutput());
                
                ($this->output)('  From:');
                foreach ($files as $file) {
                    ($this->output)('  - '.$file);
                }
                
                ($this->output)('  Using:');
                foreach ($globals as $file) {
                    ($this->output)('  - '.$file);
                }
                
                try
                {
                    $css = [];
                    foreach ($files as $file) {
                        foreach ($globals as $global) {
                            if (self::stringStartsWith(dirname($file), dirname($global))) {
                                $tmp = file_get_contents($this->projectDir.$global);
                                $tmp = str_replace("\r", '', $tmp);
                                $tmp = str_replace("\n", '', $tmp);
                                
                                $compiler->parse($tmp);
                            }
                        }
                        
                        $compiler->parseFile($file);
                        
                        $css[] = $compiler->getCss();
                        $compiler->reset();
                    }
                    
                    
                    // Create output directory if missing.
                    $this->fs->mkdir(dirname($this->projectDir.$target->getOutput()));
                    
                    $outputPath = $this->projectDir.$target->getOutput();
                    $outputContent = $target->getHeader()."\n".implode("\n\n", $css) . "\n";
                    
                    if (file_put_contents($outputPath, $outputContent) !== false) {
                        ($this->output)('  Success.');
                    }
                    else {
                        ($this->output)('  Failed.');
                    }
                }
                catch (\Exception $ex)
                {
                    ($this->output)('  An error occured:');
                    ($this->output)("\n");
                    
                    ($this->output)(self::formatLessExceptionMessage($ex));
                }
                
            }
        }
        catch (\Exception $ex)
        {
            ($this->output)(' An error occured: '.$ex->getMessage());
        }
        
        
        ($this->output)('');
        ($this->output)('<info>Done.</info>');
    }
    
    
    
    
    //========================================================================================================
    // Helpers
    //========================================================================================================
    
    private function findCompilableSources(ConfigTarget $target)
    {
        return $this->getIncludedFiles($target, 'less');
    }
    
    
    private function findCompilableGlobals(ConfigTarget $target)
    {
        return $this->getIncludedFiles($target, 'gless');
    }
    
    
    private function getIncludedFiles(ConfigTarget $target, string $extension) : array
    {
        $results = [];
        
        foreach ($target->getIncludes() as $include) {
            $path = $this->projectDir.$include;
            
            if (is_file($path)) {
                if (self::checkFilename($path, $extension)) {
                    $results[$path] = $include;
                }
            }
            elseif (is_dir($path)) {
                $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
                foreach ($files as $path => $file) {
                    if (self::checkFilename($path, $extension)) {
                        $results[$path] = substr($path, strlen($this->projectDir));//echo "$name\n";
                    }
                }
                
                usort($results, [self::class, 'sortFilesHierarchically']);
            }
            else {
                throw new InvalidArgumentException("Missing LESS file or directory : $include");
            }
        }
        
        return $results;
    }
    
    
    
    private static function checkFilename(string $filename, string $extension) : bool
    {
        return pathinfo($filename, PATHINFO_EXTENSION) === $extension;
    }
    
    
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function sortFilesHierarchically(string $a, string $b) : int
    {
        $fragmentsA = explode(DIRECTORY_SEPARATOR, $a);
        $fragmentsB = explode(DIRECTORY_SEPARATOR, $b);
        
        if (count($fragmentsA) > count($fragmentsB)) {
            return 1;
        }
        
        if (count($fragmentsA) < count($fragmentsB)) {
            return -1;
        }
        
        return strcmp($a, $b);
    }
    
    
    private static function stringStartsWith(string $source, string $needle) : bool
    {
        return mb_substr($source, 0, mb_strlen($needle)) === $needle;
    }
    
    
    private static function formatLessExceptionMessage(\Exception $ex)
    {
        //TODO: <fg=black;bg=red></>
        
        $lines = explode("\n", $ex->getMessage());
        $lines = array_map(function($line) { return chunk_split($line, 75, "\n"); }, $lines);
        $msg = '    '.implode("", $lines);
        
        return str_replace("\n", "\n    ", $msg);
    }
    
    
    
}
