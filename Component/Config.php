<?php

/*
 * This file is part of the mediagone/dotless package.
 *
 * (c) Bruce Suire <bruce@mediagone.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mediagone\Dotless\Component;


/**
 * @author Bruce Suire <bruce@mediagone.fr>
 */
class Config
{
    //========================================================================================================
    // Properties
    //========================================================================================================
    
    private $targets = [];
    
    public function getTargets()
    {
        return $this->targets;
    }
    public function addTarget(ConfigTarget $target)
    {
        $this->targets[] = $target;
    }
    
    
    
    //========================================================================================================
    // Constructor
    //========================================================================================================
    
    /**
     * @param ContainerInterface $container The injected service container instance.
     */
    public function __construct(string $json)
    {
        $data = json_decode($json);
        if ($data === null) {
            throw new \Exception("Malformed json config");
        }
        
        if (isset($data->targets) && is_array($data->targets)) {
            foreach ($data->targets as $target)
            {
                $this->addTarget(new ConfigTarget($target));
            }
        }
    }
    
    
    
}
