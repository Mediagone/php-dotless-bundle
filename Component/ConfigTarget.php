<?php

/*
 * This file is part of the mediagone/dotless package.
 *
 * (c) Bruce Suire <bruce@mediagone.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mediagone\Dotless\Component;


/**
 * 
 */
class ConfigTarget
{
    //========================================================================================================
    // Properties
    //========================================================================================================
    
    private $output;
    
    public function getOutput()
    {
        return $this->output;
    }
    public function setOutput(string $output)
    {
        $this->output = $output;
    }
    
    
    private $header;
    
    public function getHeader()
    {
        return $this->header;
    }
    public function setHeader(string $header)
    {
        $this->header = $header;
    }
    
    
    private $includes;
    
    public function getIncludes()
    {
        return $this->includes;
    }
    public function setIncludes(array $includes)
    {
        $this->includes = $includes;
    }
    
    private $excludes;
    
    public function getExcludes()
    {
        return $this->excludes;
    }
    public function setExcludes(array $excludes)
    {
        $this->excludes = $excludes;
    }
    
    
    
    //========================================================================================================
    // Constructor
    //========================================================================================================
    
    /**
     * @param array $data The config data.
     */
    public function __construct(\stdClass $data)
    {
        $this->setOutput((string)$data->output);
        $this->setHeader((string)$data->header);
        $this->setIncludes(isset($data->includes) ? $data->includes : []);
        $this->setExcludes(isset($data->excludes) ? $data->excludes : []);
    }
    
    
    
    
    
    //========================================================================================================
    // Helpers
    //========================================================================================================
    
    
}
