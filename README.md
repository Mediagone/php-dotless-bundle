PHP dotless
===========

A console LESS compiler for Symfony projects, configurable through a `.dotless` config file.


Why .dotless?
-------------

* NO third-party LESS compiler
* EASY configuration (allows multiple output files)


Installation with Composer
--------------------------

```shell
php composer require mediagone/dotless
```


Configuration
-------------
Add your LESS configuration to a `.dotless` file at the root of your project.

A default configuration could be:
```json
{
  "targets": [
    {
      "header": "/*****************************************/\n/* My compiled styles.\n/*****************************************/",
      "output": "public/styles.css",
      "includes": ["templates/"],
    }
  ]
}
```


The above example will compile any `.less` files found in the `templates/` directory into the `public/styles.css` file.

A special type of file (`.gless`) can be used to define variables in each LESS file, according to their relative hierarchy:
```
dir/
    other_vars.gless
    other_styles.less
vars.gless
styles.less
```
In the previous example, `vars.gless` will be available in both `styles.less` and `other_styles.less`, but `other_vars.gless` will only be available in the later.



Usage
-----

Compile your LESS using the following symfony's command :

```
php bin/console dotless:compile
```


You can minify the output CSS files using the `--minify` option:

```
php bin/console dotless:compile --minify
```

