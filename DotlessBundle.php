<?php

/*
 * This file is part of the mediagone/dotless package.
 *
 * (c) Bruce Suire <bruce@mediagone.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mediagone\Dotless;

use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Dotless compiler Bundle.
 */
class DotlessBundle extends Bundle
{
    
    
}
